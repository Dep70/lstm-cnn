from lstm_cell import LSTM_cell
import numpy as np
import matplotlib.pyplot as plt



def calculate_output_error(truth, output, output_weights):
    output_error = list()
    activation_error = list()
    weights = output_weights

    for i in range(1, len(output)+1):
        output_error.append(output-truth)
        activation_error.append(np.matmul(output_error, weights.T))

    return output_error, activation_error


class LSTM:

    def __init__(self, in_data, learning_rate, hidden_layers):
        self.in_data = in_data
        weights = dict()
        self.hidden_layers = hidden_layers
        self.learning_rate = learning_rate
        self.at = np.zeros([in_data.shape[0], hidden_layers], dtype=np.float32)
        self.ct = np.zeros([in_data.shape[0], hidden_layers], dtype=np.float32)
        weights['forget'] = np.random.normal(0, .01, (in_data.shape[0], hidden_layers))
        weights['input'] = np.random.normal(0, .01, (in_data.shape[0], hidden_layers))
        weights['output_gate'] = np.random.normal(0, .01, (in_data.shape[0], hidden_layers))
        weights['output'] = np.random.normal(0, .01, (in_data.shape[0], hidden_layers))
        weights['gate'] = np.random.normal(0, .01, (in_data.shape[0], hidden_layers))
        self.cell = LSTM_cell(in_data, weights, self.ct)

    def forward_prop(self):
        input_in = self.in_data
        cell_cache = list()
        out_cache = list()
        lstm_activation = list()

        cell_cache.append(self.ct)
        out_cache.append(self.at)
        for i in range(len(input_in)):
            # propagate via LSTM cell
            out, cell, act = self.cell.lstm_cell(input_in)
            out_cache.append(out)
            cell_cache.append(cell)
            lstm_activation.append(act)
            self.ct = cell_cache[i]
            self.at = out_cache[i]
        return out_cache, cell_cache, lstm_activation

    def back_prop(self, truth, out_cache , cell_cache, activation):
        # Calculate output error derivative
        output_err, out_activ_error = calculate_output_error(truth, out_cache, self.weights['output'])
        # Calculate outgate error derivative
        # Calculate gate out error derivative
        # Calculate forget out error derivative
        # Calculate input out error derivative
        return

    def train(self,data, iterations):
        return

    def predict(self):
        return

def main():
    learnrate = 0.01
    hidden = 1
    x = list(np.arange(1.0, 10.0,0.1))
    indata = np.sin(x)
    plt.plot(x, indata)
    plt.show()
    net = LSTM(indata, learnrate, hidden)
    ocache, cellcache, actcache = net.forward_prop()
    plt.plot(ocache)
    plt.show()
    #print(ocache)
    #print(cellcache)
    #print(actcache)



if __name__ == "__main__":
    main()
