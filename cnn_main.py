# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 13:17:39 2020

@author: 1522982020C
"""

import cost_functions as cf
import con_types as ct
import nn_cells as nnc
import activation as act
import numpy as np

def forward_prop_conv2D(data, filts, bias, theta3, bias3):
    #Convolution layer
    w,y = nnc.conv_cell_2D(data,filts,bias,2,0)
    y = act.relu_activation(y)
    #Pooling Layer
    pooled = ct.maxpool(y,2,2)
    fc = pooled.reshape((int((y.shape[1]/2)*(y.shape[1]/2)*y.shape[0]),1))
    #print(theta3.dot(fc).shape)
    out = theta3.dot(fc) + bias3
    return(y, fc, out)

def sgd_back_prop_conv2D(data, label, filts, bias, fc, P, cost):
    d_out = P-label
    d_theta3 = d_out.dot(fc.T)
    #d_bias3 = sum(d_out.T).T.reshape((10,1))
    return[d_filts, d_bias, d_theta3, d_bias3, cost, acc]

def CNN(data, filts, bias, theta3, bias3):
    y,fc,out = forward_prop_conv2D(data,filts,bias,theta3,bias3)
    
    cost,P = cf.softmax_cost(y,label)
    if np.argmax(out) == np.argmax(label):
        acc = 1
    else:
        acc = 0

    #= sgd_back_prop_conv2D(filts,bias,data,label,fc,P,cost)
    return

def momentumGradientDescent(data, R, W, L, mu, filts, bias, theta3, bias3, cost, acc):
    ## add description of inputs
    x = data[:,0:-1].reshape(len(data),L,W,W)
    y = data[:,-1]
    
    num_correct = 0
    cost_temp = 0
    size = len(data)
    d_filts = {}
    d_bias = {}
    v = {}
    b_v = {}
    
    for i in range(0,len(filts)):
        d_filts[i] = np.zeros(filts[0].shape)
        d_bias[i] = 0
        v[i] = np.zeros(filts[0].shape)
        b_v[i] = 0
        
    d_theta3 = np.zeros(theta3.shape)
    d_bias3 = np.zeros(bias3.shape)
    v_3 = np.zeros(theta3.shape)
    b_v_3 = np.zeros(bias3.shape)
    
    for i in range(0,size):
        image = x[i]
        label = np.zeros((theta3.shape[0],1))
        label[int(y[i]),0] = 1
        
        [d_filts_temp, d_bias_temp, d_theta3_temp, d_bias3_temp, cost_temp, acc_temp] = CNN(image, label, filts, bias, theta3, bias3)
        for j in range(0,len(filts)):
            d_filts[j] += d_filts_temp[j]
            d_bias[j] += d_bias3_temp[j]
        d_theta3 +=d_theta3_temp
        d_bias3 += d_bias3_temp
        cost += cost_temp
        num_correct += acc_temp
        
    for i in range(0,len(filts)):
        v[i] = mu*v_3-R*d_filts[i]/size
        filts[i] += v[i]
        b_v[i] = mu*b_v[i]-R*d_bias[i]/size
        bias[i] += b_v[i]
        
    v_3 = mu*v_3-R*d_theta3/size
    theta3 += v_3
    b_v_3 = mu*b_v_3-R*d_bias3/size
    bias3 += b_v_3
    
    cost_temp = cost_temp/size
    cost.append(cost_temp)
    accuracy = float(num_correct)/size
    acc.append(accuracy)
    
    return[filts, bias, theta3, bias3, cost, acc]

def init_bias(K, F):
    return(0.01*np.random.rand(K, F, F))

def init_theta(num_out, K):
    return(0.01*np.random.rand(num_out, K))

def init_filt_lacun(K, F, scale=1.0, filter_type='normal'):
    if filter_type not in {'normal'}:
        raise ValueError('Invalid filter type:'
                         'expected one of {"normal"}'
                         'but got ', filter_type)
        
    stdev = scale*np.sqrt(1/(K*F*F))
    #filt = np.random.normal(0,stdev,(K,F,F))
    filt = getattr(np.random, filter_type)(0,stdev,(K,F,F))
    return(filt)

def init_params(in_size, out_size, K, F):
    data = np.array(np.ones((K,in_size,in_size)))
    label = np.array(np.ones((K,out_size)))

    filts = np.array(np.ones((K,F,F)))
    filts = init_filt_lacun(K,F)

    bias = np.array(np.zeros((K,F,F)))
    bias = init_bias(K,F)
    bias3 = init_bias(int((out_size/2)*(out_size/2)*K),1)

    theta3 = init_theta(out_size,int((out_size/2)*(out_size/2)*K))
    return(data, label, filts, bias, bias3, theta3)

# in_size is the dimension of the input -> assumes square
in_size = 128
# out_size is the dimension of the output -> assumes square
out_size = 64
# K is both the depth of the dataset and the number of filters
K = 2
# F is the filter size -> assumes square filter
F = 2

#print(filts.shape)
data,label,filts,bias,bias3,theta3 = init_params(in_size, out_size, K, F)

y,fc,out = forward_prop_conv2D(data,filts,bias,theta3,bias3)
cost,P = cf.softmax_cost(out,label)

if np.argmax(out) == np.argmax(label):
    acc = 1
else:
    acc = 0
print(label.shape)
print(P.shape)
d_filts,d_bias,d_theta3,d_bias3,cost,acc = sgd_back_prop_conv2D(data, label, filts, bias, fc, P, cost)
