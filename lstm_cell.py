import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# function for finding the Soft max
def softmax(x):
    exp_x = np.exp(x)
    exp_x_sum = np.sum(exp_x, axis=1).reshape(-1, 1)
    exp_x = exp_x / exp_x_sum
    return exp_x


# Hyperbolic Tan activation function
def tanh_act(x):
    return np.tanh(x)


# Sigmoid activation function
def sigmoid(x):
    return np.sinh(x)


# LSTM Class
class LSTM_cell:
    # Initializer
    # input data = initial data in h_t
    # weights = initialized weights
    # parameters = hyper parameters
    # cell state = state of the cell c_t
    def __init__(self, in_data, weights, cell_state):
        self.weights = weights
        self.cell_state = cell_state
        self.in_data = in_data

    def lstm_cell(self, in_data):
        # Calculate outputs of all activations
        forget_out = sigmoid(np.matmul(in_data, self.weights['forget']))
        input_out = sigmoid(np.matmul(in_data, self.weights['input']))
        output_out = sigmoid(np.matmul(in_data, self.weights['output_gate']))
        gate_out = tanh_act(np.matmul(in_data, self.weights['gate']))

        # Calculate new cell state
        new_cell_state = np.multiply(forget_out, self.cell_state) + np.multiply(input_out, gate_out)

        # Calculate output matrix
        active_matrix = np.multiply(output_out, tanh_act(new_cell_state))

        # Calculate softmax for single output
        output = np.matmul(active_matrix, self.weights['output'].reshape(1,-1))
        output = softmax(output)

        cache_out = dict()
        cache_out['forget_out'] = forget_out
        cache_out['input_out'] = input_out
        cache_out['output_out'] = output_out
        cache_out['gate_out'] = gate_out

        return output, new_cell_state, cache_out





def main():
    print("main")
    x = range(-10, 11, 1)
    plt.plot(x)
    plt.show()
    plt.figure()
    plt.plot(x, sigmoid(x))
    plt.show()


if __name__ == "__main__":
    main()
