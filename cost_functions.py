# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 11:00:46 2020

@author: 1522982020C
"""

import numpy as np

def softmax_cost(y,label):
    #e_y = [1,1]
    print(y.shape)
    e_y = np.exp(y)
    P = e_y/sum(e_y)
    P = P.reshape(P.shape[0],P.shape[1])
    print(P.shape)
    p = sum(label*P.T)
    return(-np.log(p),P)