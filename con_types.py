# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 11:33:29 2020

@author: 1522982020C
"""

import numpy as np

def average(x):
    return(np.sum(x)/len(x))

def maxpool(x,F=1,S=1,P=0):
    ## x is the input array
    ## F is the spatial extent of the kernels (kernel size)
    ## S is the stride of convolution
    ## P is the amount of zero-padding
    
    L_in,W_in,H_in = x.shape
    W_out = int(1+(W_in-(F)+2*P)/S)
    H_out = int(1+(H_in-(F)+2*P)/S)
    #print(W_out)
    #print(H_out)
    #print(L_in)
    #print(x.shape)
    pool = np.zeros((L_in,W_out,H_out))
    #print(pool)
    for k in range(L_in):
        for i in range(W_out):
            for j in range(H_out):
                i_start = i+i*(S-1)
                i_end = i+i*(S-1)+F
                j_start = j+j*(S-1)
                j_end = j+j*(S-1)+F
                pool[k,i,j] = np.max(x[k,i_start:i_end,j_start:j_end])
    return(pool)
