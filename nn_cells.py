# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:58:44 2020

@author: 1522982020C
"""
import numpy as np
import matplotlib.pyplot as plt
import activation as act
import con_types as ct

def conv_cell_1D(x,F=1,S=1,K=1,P=0,L=0,init_type='normal'):
    # x is the input array
    # F is the spatial extent of the kernels (kernel size)
    # S is the stride of convolution
    # K is the number of filters
    # P is the amount of zero-padding
    # L is the number of layers desired in output matrix
    
    if x.ndim>1:
        raise ValueError('Invalid input: Input matrix must be at least 2D. Use conv_cell_1D for 1D arrays.')
    
    x = x.reshape((1,1,x.shape[0]))
    L_in,W_in,H_in = x.shape
    #print(x.shape)
    
    if not((1+(H_in-F+2*P)/S).is_integer()):
        raise ValueError('Invalid parameters: (X_dim-F+2P)/S must be an integer')
    
    return

def conv_cell_2D(x,filts,bias,S=1,P=0):
    ## x is the input array
    ## filt is the filter array
    ## bias is the bias array
    ## S is the stride of convolution
    ## P is the amount of zero-padding
    if filts.ndim < 3:
        raise ValueError('Invalid input: Filter matrix must have 3 dimensions.')

    K = filts.shape[0]
    F = filts.shape[1]

    if x.ndim == 1:
        raise ValueError('Invalid input: Input matrix must be at least 2D. Use conv_cell_1D for 1D arrays.')
    
    if x.ndim == 2:
        x = x.reshape((1,x.shape[0],x.shape[1]))      
    L_in,W_in,H_in = x.shape
    
    if not((1+(W_in-F+2*P)/S).is_integer()) or not((1+(H_in-F+2*P)/S).is_integer()):
        raise ValueError('Invalid parameters: (X_dim-F+2P)/S must be an integer')
    
    out_dim = np.sqrt(L_in*W_in*H_in/K)
    W_out = int(1+(out_dim-F+2*P)/S)
    H_out = W_out
    L_out = K
       
    y = np.array(np.zeros((L_out,H_out,W_out)))

    for k in range(L_in):
        for i in range(H_out):
            for j in range(W_out):
                i_start = i+i*(S-1)
                i_end = i+i*(S-1)+F
                j_start = j+j*(S-1)
                j_end = j+j*(S-1)+F
                temp = np.array(np.zeros((F,F)))
                temp = x[k,i_start:i_end,j_start:j_end]
                y[k,i,j] = np.sum(temp*filts[k]+bias[k])
    return(filts,y)
       