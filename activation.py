# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-5,5,50)

def sigmoid_activation(x):
    return(1/(1+np.exp(-x)))

def tanh_activation(x):
    return(np.tanh(x))

def relu_activation(x):
    x[x<0] = 0
    return(x)

#plt.plot(x,sigmoid_activation(x))
#plt.plot(x,tanh_activation(x))
#plt.plot(x,relu_activation(x))